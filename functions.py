###############################################################################
####################             Functions needed          ####################
####################            Ioannis Papazoglou         ####################
####################            UNIGE Summer 2023          ####################
###############################################################################

# from functions import function1, function2
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np

def save_df2excel(data,path,file_name):
    from datetime import datetime
    import os
    # Check if the CSV file already exists
    output_file_path = path+file_name+'.xlsx'
    if not(os.path.isfile(output_file_path)):  
        data.to_excel(output_file_path, index_label='account_id')
        print(f"Excel sheet saved to: {output_file_path}.")

    else:
        print(f"Warning: {output_file_path} already exists. Data will not be overwritten.")
        # Prepare the file name with the current date and time
        current_datetime = datetime.now().strftime("%Y%m%d_%H%M%S")
        output_file_path = path+file_name+current_datetime+'.xlsx'
        # Save the time-series DataFrame to a CSV file
        data.to_excel(output_file_path, index_label='account_id')
        # wide_df.to_csv(csv_file, index=False)
        print(f"Time-series data saved to {output_file_path}.")
 
def save_df2excel_multisheet(data_dict, path, file_name):
    from datetime import datetime
    import os
    # data_dict={'Sheet1': df1, 'Sheet2': df2}
    # Check if the Excel file already exists
    output_file_path = os.path.join(path, file_name + '.xlsx')

    if not os.path.isfile(output_file_path):
        writer = pd.ExcelWriter(output_file_path, engine='xlsxwriter')
        for sheet_name, data in data_dict.items():
            data.to_excel(writer, sheet_name=sheet_name, index_label='account_id')
        writer.save()
        print(f"Excel file saved to: {output_file_path}.")
    else:
        print(f"Warning: {output_file_path} already exists. Data will not be overwritten.")
        # Prepare the file name with the current date and time
        current_datetime = datetime.now().strftime("%Y%m%d_%H%M%S")
        output_file_path = os.path.join(path, file_name + current_datetime + '.xlsx')
        writer = pd.ExcelWriter(output_file_path, engine='xlsxwriter')
        for sheet_name, data in data_dict.items():
            data.to_excel(writer, sheet_name=sheet_name, index_label='account_id')
        writer.save()
        print(f"Excel file saved to: {output_file_path}.")        

def cat_plot_subset(data, column, h , n=10, fig_height = 10, fig_width = 8):
    # Count the occurrences of each category
    category_counts = data[column].value_counts()

    # Select the top n categories with the highest counts
    top_categories = category_counts.nlargest(n).index

    # Filter the DataFrame to include only the selected categories
    filtered_data = data[data[column].isin(top_categories)]

    # Sort the selected categories by count in descending order
    sorted_categories = filtered_data[column].value_counts().index
    
    # Set the figure size to be long and thin
    sns.set(rc={'figure.figsize': (fig_height, fig_width)})
    
    if h == '':
        # Create the catplot with sorted categories
        sns.catplot(data=filtered_data, y=column, kind='count',
                    palette="muted", order=sorted_categories)
    else:
        # Create the catplot with sorted categories
        sns.catplot(data=filtered_data, y=column, kind='count',
                    palette="muted", order=sorted_categories,hue=h)

    # Display the plot
    plt.show()

def cat_plot_subset_save(data, column, h , n=10, fig_height = 10, fig_width = 8,fig_name='figure_x'):
    # Count the occurrences of each category
    category_counts = data[column].value_counts()

    # Select the top n categories with the highest counts
    top_categories = category_counts.nlargest(n).index

    # Filter the DataFrame to include only the selected categories
    filtered_data = data[data[column].isin(top_categories)]

    # Sort the selected categories by count in descending order
    sorted_categories = filtered_data[column].value_counts().index
    
    # Set the figure size to be long and thin
    sns.set(rc={'figure.figsize': (fig_height, fig_width)})
    
    if h == '':
        # Create the catplot with sorted categories
        sns_plot = sns.catplot(data=filtered_data, y=column, kind='count',
                    palette="muted", order=sorted_categories)
    else:
        # Create the catplot with sorted categories
        sns_plot = sns.catplot(data=filtered_data, y=column, kind='count',
                    palette="muted", order=sorted_categories,hue=h)

    # Save the plot with good resolution
    sns_plot.figure.savefig('figures/'+fig_name+'.png', dpi=400)
    # Display the plot
    plt.show()


def ridge_plot_over_years(df,y,x):
    sns.set_theme(style="white", rc={"axes.facecolor": (0, 0, 0, 0)})

    pal = sns.cubehelix_palette(10, rot=-.25, light=.7)
    g = sns.FacetGrid(df, row=y, hue=y, aspect=15, height=.5, palette=pal)

    # Draw the densities in a few steps
    g.map(sns.kdeplot, x,
        bw_adjust=.5, clip_on=False,
        fill=True, alpha=1, linewidth=1.5)
    g.map(sns.kdeplot, x, clip_on=False, color="w", lw=2, bw_adjust=.5)

    # passing color=None to refline() uses the hue mapping
    g.refline(y=0, linewidth=2, linestyle="-", color=None, clip_on=False)

    # Define and use a simple function to label the plot in axes coordinates
    def label(x, color, label):
        ax = plt.gca()
        ax.text(0, .2, label, fontweight="bold", color=color,
                ha="left", va="center", transform=ax.transAxes)
    
    # set the years as y labels
    g.map(label, x)
    # Set the subplots to overlap
    g.figure.subplots_adjust(hspace=-.25)
    # Remove axes details that don't play well with overlap
    g.set_titles("")
    g.set(yticks=[], ylabel="")
    g.despine(bottom=True, left=True)


def plot_empty_data(df,width,height):
    fig, ax = plt.subplots(figsize=(width,height)) 
    sns.heatmap(df.isnull(), cbar=False)
    # Add title
    ax.set_title("Heatmap of Missing Values")
    # Add axis labels
    ax.set_xlabel("Parameters.")
    ax.set_ylabel("Observations.")
    plt.show()

def drop_rows(df, column_name, string_value):
    # Print length before removal
    before_length = len(df)
    print("Length before removal:", before_length)
    
    # Drop rows with missing values in the specified column
    df = df.dropna(subset=[column_name])

    # Drop rows with a given string value in the specified column
    df = df[~df[column_name].isin(string_value)]

    # Print length after removal
    after_length = len(df)
    print("Length after removal:", after_length)

    # Calculate percentage removed
    percentage_removed = (before_length - after_length) / before_length * 100
    print("Percentage removed: {:.2f}%".format(percentage_removed))

    return df

def text2number(df,column_name):
    df[column_name] = df[column_name].astype(float)
    return df