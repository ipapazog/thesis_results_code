# Impact of Internal Carbon Pricing Adoption on Corporate Environmental Outcomes

## MSc Thesis in Economics - GSEM - University of Geneva
## By Ioannis Papazoglou with Supervisor: Julian Daubanes and Jury: Emmanuel Milet


### Contents of this Repository as of Sep 2023:

#### 1) main folder
Main code:
- The final code with the resuts prsented can be seen in the python notebook: 2_thesis_code.
- The descriptive statistics and some preliminary data pre-processing can be found in the notebook: 1_descriptive_statistics.

#### 2) data
In the folder data I save and retrieve the various datasets I am using:
- Initial datasets.
- Pre-processed datasets from different pieces of code.

#### 3) figures
In the folder figures, I saved some of the figures I used in the thesis and some old ones

#### 4) results
The resuls folder holds various results files from the regressions.
All the resutls used in the thesis are there but additional result files are also present.
The result files used in thesis are:
- ALL_2_9.xlsx : overview of all the results used
- log_Covariates_Check.csv
- ICP_Interactions_3models.csv
- REAL_AND_IMMAGINARY_covariates.csv

#### 5) py_files:
Further, in the folders py_files you can see the code I used to download data from Refinitiv, extract information from the CDP questionnaire and do some initial pre-processing.

#### 6) notebooks_not_in_thesis: 
The folder notebooks_not_in_thesis includes various notebooks used while development but not in the final results.
It includes:
- Further descriptive statistics.
- Additional results with TWFE not presented in the thesis.
- Impuning of missing data using different techniques.
- Matching of control and treatment groups using PSM.
- Initial experimentation with DiD and TWFE.

#### 7) tmp:
Folder used for some of the machine learing tools I tried for impuning.
